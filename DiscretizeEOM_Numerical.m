%% Load data from eom1

clear
clc
load('eom1data 20160407T090045')


%% Euler Discrete A,B

syms T k real
assumeAlso(k,'integer')
assumeAlso(T>0)

A_func = matlabFunction(A_trj);

Ad = T*Afunc(k*T)


%% Euler Approximation of x(k+1)

syms T k real
assumeAlso(k,'integer')
assumeAlso(T>0)

x_next = A(k*T)*x(k)



%% Euler Approximation of State Transition Matrix

syms k
assume(k,'integer')

A_func = matlabFunction(A_trj);
STM{1} = eye(10);

STM_func_maker = @(T) matlabFunction(  A_func(k*T)*STM{k}   );
% STM_func = @(k) A_func(k*T)*STM{k};
STM_func = STM_func_maker(T);







