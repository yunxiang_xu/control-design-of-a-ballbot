%% Load data from eom1

clear
clc
load('eom1data 20160408T113722')


%% System Discretization: Minimum Realization (Gilbert's Method)

syms s clear


C_trj = eye(10);
D_trj = zeros(size(C_trj,1),size(B_trj,2));
G = C_trj*(s*eye(10) - A_trj)\B_trj + D_trj;
% G = simplify(G);


%% Gilbert's Method Continued

[Gnum_all,Gden_all] = numden(G);

% MANUAL VERSION OF PULLING OUT THE DENOMINATOR OF G
% 
% Gden_manual = (7*s*(s^2 + 4*pi^2)*(87794767909837587234384146350323783414262324910799855761664712336607121768448000*pi + ...
%     609863802803953057271737058829804153283093877875732036378435739012283323240281600*cos(4*pi*t) - ...
%     28395621205607973836864084478346005810712815824756586792225053370077384015872000*pi*cos(4*pi*t) + ...
%     226700250039713071679862321708870810026747629254525960322126790871063666402263040*s*sin(4*pi*t) + ...
%     260467183175518649517910987354951355706770111212242035656960642975426838265856*pi^2*cos(4*pi*t) - ...
%     805323318318797031050038826295430119472299057290430532835438911574113993621504*pi^2 + ...
%     5097402616425940128320732488900837623007193722420979754708309171288547834986496*s^2*cos(4*pi*t) - ...
%     65213479510751802626711927246479507204901313261161443454127374417881466291617792*s^2 + ...
%     2022057675727126911545438144883749846229277875222754862288972514679868488679424*pi*s*sin(4*pi*t) - ...
%     1885602383412890695764621980024497077762104628874176358897581313308995760576292525));
% Gden_manual_factors = factor(Gden_manual);
% Gden_manual_factors = reshape(Gden_manual_factors,numel(Gden_manual_factors),1);


% THIS CODE ALLOWED ME TO DETERMINE BY INSPECTION THAT THERE ARE ONLY 4
% FACTORS IN THE DENOMINATOR OF G, AND THEY ARE ALL CONTAINED IN
% Gden_all(1,1)
% 
% my4by1 = [];
% my3by1 = [];
% my2by1 = [];
% my1by1 = [];
% 
% for l = 1:numel(Gden_all)
%     myfactors{l} = factor(Gden_all(l));
%     myfactors{l} = reshape(myfactors{l},numel(myfactors{l}),1);
%     
%     if numel(myfactors{l})==4
%         my4by1 = [my4by1,myfactors{l}];
%     elseif numel(myfactors{l})==3
%         my3by1 = [my3by1,myfactors{l}];
%     elseif numel(myfactors{l})==2
%         my2by1 = [my2by1,myfactors{l}];
%     elseif numel(myfactors{l})==1
%         my1by1 = [my1by1,myfactors{l}];
%     end
%     
% end

Gden = Gden_all(1,1);
Gden_factors = factor(Gden);
Gden_factors = reshape(Gden_factors,numel(Gden_factors),1);

p_idx = 1;
for l = 1:numel(Gden_factors)
    
    s_soln{l} = solve(Gden_factors(l),s);
    
    % THIS IS REALLY SHITTY CODE BUT MATLAB solve CAN'T FIND PURELY
    % IMAGINARY ROOTS OF POLYNOMIALS BECAUSE IT'S A DOUCHEBAG SO I'M GIVING
    % IT A HAND. WE DON'T USE vpasolve BECAUSE IT FUCKS UP FUTURE STEPS
%     if l==3
%         s_soln{l} = [sym(-pi*2*1i);sym(2*pi*1i)];
% %         s_soln{l} = vpasolve(Gden_factors(l),s);
%     end
    % THIS IS THE END OF THE SHITTY CODE
    
    for ll = 1:numel(s_soln{l})
        Gden_poles(p_idx) = s_soln{l}(ll);
        p_idx = p_idx+1;
        
    end
end

if numel(Gden_poles)==numel(unique(Gden_poles))
    disp(' ');disp('All poles of G(s) are unique');disp(' ');
end

Gden_poles = reshape(Gden_poles,numel(Gden_poles),1);

Gcommon = G*Gden/Gden;
% for k = 1:numel(Gden_poles)
for k = 1:numel(Gden_poles)
    
% %     Method 1
%     RR{k} = limit((s-Gden_poles(k))*G,s,Gden_poles(k));
    
    
% %     Method 2
%     RR{k} = (s-Gden_poles(k))*Gcommon;
    RR{k} = (s-Gden_poles(k))*G;
    s = Gden_poles(k);
    RR{k} = subs(RR{k});
%     RR{k} = simplify(RR{k});
    syms s
    
    RRranks(k) = rank(RR{k});
end

%% Test G and s poles

syms s clear

Guse = G;
for k = 1:numel(Gden_poles)
for m = 1:size(Guse,1);
    for n = 1:size(Guse,2);
        try
            TestR{k,m,n} = (s-Gden_poles(k))*Guse(m,n);
            TestR{k,m,n} = subs(TestR{k,m,n},s,Gden_poles(k));
        catch
            TestR{k,m,n} = 'Could not Subs';
        end
    end
end
end

%% System Discretization: Continuous State Transition Matrix

% % Let X be the fundamental matrix of the continuous LTV system
% zerofiller1 = zeros(7,3);
% X_end = [zerofiller1;eye(3)];
% HailMary = [zeros(6,1);1;0;0;0];
% X = [colspace(A_trj), HailMary ,X_end];
% X = colspace(A_trj);
% Xd = diff(X,t);
% AX = A_trj*X;
% test{1} = Xd-AX;
% for l = 1:rank(X)
%     test{l+1} = Xd(:,l)-A_trj*X(:,l);
% end
% 