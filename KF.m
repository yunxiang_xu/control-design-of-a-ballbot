% KF

clc;
clear;
len = 10;
Ts = 0.0001;
total_step = 200;
x0 = zeros(len,1);
x_real = zeros(len,total_step);
x_ekf = zeros(len,total_step);
y = zeros(len-4,total_step);
u0 = [0.1;0;0];
uk = u0;
C = [zeros(6,4) eye(6)];
x_init = randn(len);
w = zeros(total_step,len);
Q = cov(w);
v = 5*wgn(total_step,len-4,10 );
R = cov(v);

% first time step
% simlation
x_real(:,1) = stateMatrix(Ts)*x0 + inputMatrix(Ts)*uk + w(1,:)';
y(:,1) = C*x_real(:,1) + v(1,:)';

% prediction
x_pre = x0;
P_pre = cov(x_init) + Q;

% filtering
K = P_pre*C'/(C*P_pre*C' + R);
x_ekf(:,1) = x_pre + K*(y(:,1) - C*x_pre);
P_ekf = (eye(len) - K*C)*P_pre;

% time simulation
for k = 2:total_step
    t = k*Ts;
    A = disStateM(t,Ts);
    B = disInputM(t,Ts);
    % simulation with noise
    x_real(:,k) = A*x_real(:,k-1)+B*uk + w(k,:)';
    y(:,k) = C*x_real(:,k) + v(k,:)';
    
    % predicted cycle
    x_pre = A*x_ekf(:,k-1)+B*uk;
    P_pre = A*P_ekf*A'+Q;
    
    
    % filtered cycle
    K = P_pre*C'/(C*P_pre*C' + R);
    x_ekf(:,k) = x_pre + K*(y(:,k) - C*x_pre);
    P_ekf = (eye(len) - K*C)*P_pre;
end
t_plot = Ts:Ts:t;
figure(1)
subplot(3,1,1)
plot(t_plot,x_real(7,:))
subplot(3,1,2)
plot(t_plot,x_ekf(7,:))
subplot(3,1,3)
plot(t_plot,y(3,:));