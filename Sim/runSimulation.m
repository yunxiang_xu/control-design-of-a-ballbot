clc
close all
%%
% System setup
% Parameters
n=0.1; %step size of discretized version
N=6;    %finite horizon size
time=30; %simulation end time
total_step = time/n;

computeA;
% If the parameters above remain the same, do not need to compute A again,
% it takes long time!
%%

%initial state
xsi = zeros(1,10);
xsi(5) = -2*pi;
xsi(8) = 0.2876;
% note: this indicates the initial difference from reference trajectory

% system noise and sensor noise
w = [zeros(total_step,4) 0.01*wgn(total_step,3,1) 0.01*wgn(total_step,3,1)]; %system disturbance
Q = cov(w);
v =[0.5*wgn(total_step,3,1) 0.1*wgn(total_step,3,1)]; %sensor noise
R = cov(v);

% Run the simulation, don't run linear and nonlinear model at the same time!!

linearTrajSim;
% nonlinearTrajSim;

% plot the outputs , system states, kalman filter resutls ball position...
trajSimPlot;