%Simulations for Rolling in a Trajectory
%ME561 -- Final Project 
%Reaction Wheel Ball Bot
%Team:
    %Matthew Porter
    %Isaac Spiegel
    %Yunxiang Xu
    %Ding Zhang

close all
clc


%%
%linearized simulation

clear xout x_error input Tn  Xn x_ekf t_plot

C = [zeros(6,4) eye(6)]; %output function



ts=0;

x_error(1,:)=xsi;

% initial state for kalman filter
x_pre = xsi';

P_pre = zeros(10);

for i=1:(time/n)
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b');
    fprintf('time step %d',i);
    x_ref = [cos(pi*ts),sin(pi*ts),0,0,2*pi,0,0,-0.2876,0,0];
    A = A_dis{i};  %state matrix
    B = disB(ts,n);%input matrix
    
    
    xe_last=x_error(length(x_error(:,1)),:);
    xout(i,:)=xe_last + x_ref;
% output function
    x_state = xe_last';
    y(:,i) = C*x_state + v(i,:)';
% filtering
    K = P_pre*C'/(C*P_pre*C' + R);
    x_ekf(:,i) = x_pre + K*(y(:,i) - C*x_pre);
    P_ekf = (eye(10) - K*C)*P_pre;
    
    y(:,i) = C*(xe_last' + x_ref') + v(i,:)';
% LQR
    A_lqr = cell(N,1);
    for j = 1:N
        A_lqr{j} = A_dis{i+j-1};
    end
    input(i,:)=transpose(LQ_reg( A_lqr,ts,x_ekf(:,i)',n,N ));
    uk = input(i,:)';
% prediction
    x_pre = A*x_ekf(:,i)+B*uk;
    P_pre = A*P_ekf*A'+Q;
    
%  simulation
    [Tn,Xn] = ode45(@(t,x)a(t)*x+b(t)*transpose(input(i,:)),[ts,ts+n],xe_last);
    xn = Xn(length(Xn(:,1)),:)+w(i,:);
    ts=ts+n;
    x_error=[x_error;xn];
end
x_error = x_error(1:total_step,:);
t_plot = 0:n:time-n;
%%
%plots for Linearized simulation
fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b');
disp('plotting...');
trajSimPlot;