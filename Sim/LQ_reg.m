function [ u ] = LQ_reg( A_lqr,t,x,n,N )
%LQR Controller for Trajectory
%ME561 -- Final Project 
%Reaction Wheel Ball Bot

%LQR matricies
Q=1*[1 0 0 0 0 0 0 0 0 0;
      0 1 0 0 0 0 0 0 0 0;
      0 0 1 0 0 0 0 0 0 0;
      0 0 0 1 0 0 0 0 0 0;
      0 0 0 0 0.1 0 0 0 0 0;
      0 0 0 0 0 0.1 0 0 0 0;
      0 0 0 0 0 0 0.1 0 0 0;
      0 0 0 0 0 0 0 0.01 0 0;
      0 0 0 0 0 0 0 0 0.01 0;
      0 0 0 0 0 0 0 0 0 0.01];
  
R=40*[1 0 0;0 1 0;0 0 1];


B=@(t)b(t);
%Equation for P
P= @(Ai,Bi,Qi,Ki,Ri,Pi)[(transpose(Ai-Bi*Ki)*Pi*(Ai-Bi*Ki))+Qi+transpose(Ki)*Ri*Ki];

%Equation for K
K= @(Ai,Bi,Pi,Ri)[((transpose(Bi)*Pi*Bi+Ri)\transpose(Bi)*Pi*Ai)];

Ps=Q;
for i=1:N
    
    %calculate A
    A = A_lqr{N+1-i};
    
    %calculate B
 
    Ks=K(A,B(t+(N-i)*n)*n,Ps,R);
    Ps=P(A,B(t+(N-i)*n)*n,Q,Ks,R,Ps);
    
end

u=-Ks*transpose(x);

