
clear
clc

%% Equations of Motion for BB-8 using quaternion orientation

syms a b c d ad bd cd dd ...
     p  q  r  pd qd rd...
     w1  w2  w3 w1d w2d w3d...
     T1 T2 T3 real

syms t real
assumeAlso(t>0)
 
%small Reaction Wheel 
IL=.00014;%kg m^2
Is=0.000074;%kg m^2

%Large Reaction Wheel
IL2=0.016598277;%kg m^2
Is2=0.008327479;%kg m^2

%Ball w/out reaction wheels
Ixx=0.003802;%kg m^2
Iyy=0.005857;%kg m^2
Izz=0.005319;%kg m^2

%Rolling w/out slipping constant
mr2=3.823*0.15^2;%kg m^2
%%
%Moment applied to reaction wheel x
M1=[w1d*IL2+q*r*Is2-q*r*Is2;
    qd*Is2+r*w1*IL2-r*w1*Is;
    rd*Is2+q*w1*Is2-q*w1*IL2];

%Moment applied to reaction wheel y
M2=[pd*Is+w2*r*Is-w2*r*IL;
    w2d*IL+r*p*Is-r*p*Is;
    rd*Is+w2*p*IL-w2*p*Is];

%Moment applied to reaction wheel z
M3=[pd*Is+q*w3*IL-q*w3*Is;
    qd*Is+w3*p*Is-w3*p*IL;
    w3d*IL+q*p*Is-q*p*Is];

%Moment applied to ball from reaction wheels
Mwheels=-simplify(M1+M2+M3);

%orientation matrix world-> ball
O=[a^2+b^2-c^2-d^2 2*b*c-2*a*d, 2*b*d+2*a*c;
    2*b*c+2*a*d, a^2-b^2+c^2-d^2, 2*c*d-2*a*b;
    2*b*d-2*a*c, 2*c*d+2*a*b, a^2-b^2-c^2+d^2];
transpose(O);
R=[-mr2 0 0; 0 -mr2 0; 0 0 0];
simplify(O*R*transpose(O));
%Moment applied to ball based on rolling w/o slipping
Mroll=simplify(O*R*transpose(O)*[pd;qd;rd]);

%Moment applied to Ball based on motion of ball
Mb=[pd*Ixx+q*r*(Izz-Iyy);
    qd*Iyy+p*r*(Ixx-Izz);
    rd*Izz+p*q*(Iyy-Ixx)];

%solve for EOM
eom_soln = solve(Mb==Mwheels+Mroll,[pd qd rd]);

%Declare states 
%a b c d: quaternion values
%p q r: body fixed angular velocity of ball
%w1 w2 w3: angular velocity of reaction wheels
x=transpose([a b c d p q r w1 w2 w3]);
 
%simplify and put in terms of torque
eom=subs(simplify([eom_soln.pd;eom_soln.qd;eom_soln.rd]),[w1d w2d w3d],[T1/IL,T2/IL,T3/IL]);

%Derivative of states 
xd=[ -0.5*(x(2)*x(5)+x(3)*x(6)+x(4)*x(7));
   0.5*(x(1)*x(5)+x(4)*x(6)-x(3)*x(7));
   0.5*(x(1)*x(6)+x(2)*x(7)-x(4)*x(5));
   0.5*(x(1)*x(7)+x(3)*x(5)-x(2)*x(6));
   eom;
   [T1/IL2;T2/IL;T3/IL]];

%% Simplify EOM

% xd = simplify(xd);

xd_vpa = vpa( xd, 5 )
%% SECTION TITLE
% DESCRIPTIVE TEXT
   

%% Linearization: Jacobians

u = [T1;T2;T3];

A = jacobian(xd,x);
B = jacobian(xd,u);
% A_vpa = vpa(A,2);
% B_vpa = vpa(B,2);

% xd_mat = A*x + B*u;

%% Linearization: Trajectory
p = 2*pi;

% x-trajectory
a = cos(p*t/2);
b = sin(p*t/2);
c = 0;
d = 0;
p = p;
%% SECTION TITLE
% DESCRIPTIVE TEXT
q = 0;
r = 0;
w1 = -0.2876;
w2 = 0;
w3 = 0;
% u-trajectory
T1 = 0;
T2 = 0;
T3 = 0; 
%%
A_trj = simplify(subs(A))
B_trj = simplify(subs(B));
% A_trj = subs(A);
% B_trj = subs(B);

%% Linearization: Upright Point

% x-trajectory
a = 1;
b = 0;
c = 0;
d = 0;
p = 0;
q = 0;
r = 0;
w1 = 0;
w2 = 0;
w3 = 0;
% u-trajectory
T1 = 0;
T2 = 0;
T3 = 0;

A_pnt = simplify(subs(A));
B_pnt = simplify(subs(B));

%% Characteristic Polynomials

cp_pnt = simplify(charpoly(A_pnt));
cp_trj = simplify(charpoly(A_trj));

%% Print cp and ss
clc

A_print = vpa(A_trj,2);
B_print = vpa(B_trj,2);
cp_print = vpa(cp_trj,2);

disp('A and B matrices for the LTV system')
disp(' ')
disp('A = ')
disp(vpa(A_trj(:,1:8),2))
disp(vpa(A_trj(:,9:end),2))
disp(' ')
disp('B = ')
disp(B_print)
disp(' ')
disp(' ')
disp(' ')
disp('Characteristic Polynomial for the LTV system')
disp(' ')
disp('CP = ')
disp(vpa(cp_trj(1:4),2))
disp(vpa(cp_trj(5:end),2))

%% Save EOM

savedata = questdlg('Do you want to save data?');
if strcmp(savedata,'Yes')
save(['eomdata ',datestr(now,30)])
end

%% Other code

% RUN DiscretizeEOM.m (analytical or numerical) TO PERFORM DISCRETIZATION OPERATION

% %% System Discretization: Minimum Realization (Gilbert's Method)
% 
% syms s
% 
% C_trj = eye(10);
% D_trj = zeros(size(C_trj,1),size(B_trj,2));
% G = C_trj*(s*eye(10) - A_trj)\B_trj + D_trj;
% G = simplify(G);
% 
% 
% %% Gilbert's Method Continued
% 
% [Gnum_all,Gden_all] = numden(G);
% 
% % MANUAL VERSION OF PULLING OUT THE DENOMINATOR OF G
% % 
% % Gden_manual = (7*s*(s^2 + 4*pi^2)*(87794767909837587234384146350323783414262324910799855761664712336607121768448000*pi + ...
% %     609863802803953057271737058829804153283093877875732036378435739012283323240281600*cos(4*pi*t) - ...
% %     28395621205607973836864084478346005810712815824756586792225053370077384015872000*pi*cos(4*pi*t) + ...
% %     226700250039713071679862321708870810026747629254525960322126790871063666402263040*s*sin(4*pi*t) + ...
% %     260467183175518649517910987354951355706770111212242035656960642975426838265856*pi^2*cos(4*pi*t) - ...
% %     805323318318797031050038826295430119472299057290430532835438911574113993621504*pi^2 + ...
% %     5097402616425940128320732488900837623007193722420979754708309171288547834986496*s^2*cos(4*pi*t) - ...
% %     65213479510751802626711927246479507204901313261161443454127374417881466291617792*s^2 + ...
% %     2022057675727126911545438144883749846229277875222754862288972514679868488679424*pi*s*sin(4*pi*t) - ...
% %     1885602383412890695764621980024497077762104628874176358897581313308995760576292525));
% % Gden_manual_factors = factor(Gden_manual);
% % Gden_manual_factors = reshape(Gden_manual_factors,numel(Gden_manual_factors),1);
% 
% 
% % THIS CODE ALLOWED ME TO DETERMINE BY INSPECTION THAT THERE ARE ONLY 4
% % FACTORS IN THE DENOMINATOR OF G,
% % 
% % my4by1 = [];
% % my3by1 = [];
% % my2by1 = [];
% % my1by1 = [];
% % 
% % for l = 1:numel(Gden_all)
% %     myfactors{l} = factor(Gden_all(l));
% %     myfactors{l} = reshape(myfactors{l},numel(myfactors{l}),1);
% %     
% %     if numel(myfactors{l})==4
% %         my4by1 = [my4by1,myfactors{l}];
% %     elseif numel(myfactors{l})==3
% %         my3by1 = [my3by1,myfactors{l}];
% %     elseif numel(myfactors{l})==2
% %         my2by1 = [my2by1,myfactors{l}];
% %     elseif numel(myfactors{l})==1
% %         my1by1 = [my1by1,myfactors{l}];
% %     end
% %     
% % end
% 
% 
%     
% Gden = Gden_all(1,1);
% Gden_factors = factor(Gden);
% Gden_factors = reshape(Gden_factors,numel(Gden_factors),1);
% 
% p_idx = 1;
% for l = 1:numel(Gden_factors)
%     s_soln = solve(Gden_factors(l),s);
%     for ll = 1:numel(s_soln)
%         Gden_poles(p_idx) = s_soln(ll);
%         p_idx = p_idx+1;
%     end
% end
% 
% if numel(Gden_poles)==numel(unique(Gden_poles))
%     disp(' ');disp('All poles of G(s) are unique');disp(' ');
% end
% 
% Gden_poles = reshape(Gden_poles,numel(Gden_poles),1);
% 
% for k = 1:numel(Gden_poles)
%     R(k) = (s-Gden_poles(k))*G;
%     s = Gden_poles(k);
%     R(k) = simplify(subs(R(k)));
%     syms s
%     Rranks(k) = rank(R(k));
% end
% 
% %% System Discretization: Continuous State Transition Matrix
% 
% % % Let X be the fundamental matrix of the continuous LTV system
% % zerofiller1 = zeros(7,3);
% % X_end = [zerofiller1;eye(3)];
% % HailMary = [zeros(6,1);1;0;0;0];
% % X = [colspace(A_trj), HailMary ,X_end];
% % X = colspace(A_trj);
% % Xd = diff(X,t);
% % AX = A_trj*X;
% % test{1} = Xd-AX;
% % for l = 1:rank(X)
% %     test{l+1} = Xd(:,l)-A_trj*X(:,l);
% % end
% % 
% 
% 
% 
% 
% 
