% KF

clc;
close all;
len = 10;
Ts = 0.01;
total_step = 300;
x0=[-0.5 0.2 0 0.1 0 0.2 0 0 0.1 0]'; %initial state of no motion;
x_real = zeros(len,total_step);
x_ekf = zeros(len,total_step);
y = zeros(len-4,total_step);
u0 = [0;0;0];
uk = [0;0;0];
C = [zeros(6,4) eye(6)];
w = [zeros(total_step,4) 0.01*wgn(total_step,3,1) 0.01*wgn(total_step,3,1)];
Q = cov(w);
v =[0.1*wgn(total_step,3,1) 0.1*wgn(total_step,3,1)];
R = cov(v);

% first time step
% simlation
x_real(:,1) = disA(0,Ts)*x0 + disB(0,Ts)*u0 + w(1,:)';
y(:,1) = C*x_real(:,1) + v(1,:)';

% prediction
% x_pre = x0;
x_pre = [0 0 0 0 0 0.2 0 0 0.1 0]'; 
P_pre =  0.5*eye(10);

% filtering
K = P_pre*C'/(C*P_pre*C' + R);
x_ekf(:,1) = x_pre + K*(y(:,1) - C*x_pre);
P_ekf = (eye(len) - K*C)*P_pre;

% time simulation
for k = 2:total_step
    fprintf ('\b\b\b\b\b\b\b\b\b\b\b\b\b');
    fprintf ('time step %d', k);
    t = k*Ts;
    A = disA(t,Ts);
    B = disB(t,Ts);
    % simulation with noise
    x_real(:,k) = A*x_real(:,k-1)+B*uk + 0*w(k,:)';
    y(:,k) = C*x_real(:,k) + v(k,:)';
    
    % predicted cycle
    x_pre = A*x_ekf(:,k-1)+B*uk;
    P_pre = A*P_ekf*A'+Q;
    
    
    % filtered cycle
    K = P_pre*C'/(C*P_pre*C' + R);
    x_ekf(:,k) = x_pre + K*(y(:,k) - C*x_pre);
    P_ekf = (eye(len) - K*C)*P_pre;
end
t_plot = Ts:Ts:t;
figure(1)
subplot(2,1,1)
plot(t_plot,x_real(1,:),t_plot,x_real(2,:),t_plot,x_real(3,:),t_plot,x_real(4,:));
h=legend('$\delta a$','$\delta b$','$\delta c$','$\delta d$');
set(h,'Interpreter','latex')
title('Quarternion Valuse Error')

xlim([0,t]);
ylim([-1 1]);
subplot(2,1,2)
plot(t_plot,x_ekf(1,:),t_plot,x_ekf(2,:),t_plot,x_ekf(3,:),t_plot,x_ekf(4,:));
h=legend('$\delta a$','$\delta b$','$\delta c$','$\delta d$');
set(h,'Interpreter','latex')
title('Quarternion Values Error (KF)')

xlim([0,t]);
ylim([-1 1]);

figure(2)
subplot(3,1,1)
plot(t_plot,x_real(5,:),t_plot,x_real(6,:),t_plot,x_real(7,:));
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Ball Angular Velocity Error');
ylim([-0.5 0.5]);
subplot(3,1,2)
plot(t_plot,x_ekf(5,:),t_plot,x_ekf(6,:),t_plot,x_ekf(7,:));
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Ball Angular Velocity Error (KF)')
ylim([-0.5 0.5]);
subplot(3,1,3)
plot(t_plot,y(1,:),t_plot,y(2,:),t_plot,y(3,:));
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Ball Angular Velocity Error (Gyro)')
ylim([-0.5 0.5]);

figure(3)
subplot(3,1,1)
plot(t_plot,x_real(8,:),t_plot,x_real(9,:),t_plot,x_real(10,:));
h = legend('$\delta w1$','$\delta w2$','$\delta w3$');
set(h,'Interpreter','latex');
title('Wheels Angular Velocity Error');
ylim([-0.5 0.5]);
subplot(3,1,2)
plot(t_plot,x_ekf(8,:),t_plot,x_ekf(9,:),t_plot,x_ekf(10,:));
h = legend('$\delta w1$','$\delta w2$','$\delta w3$');
set(h,'Interpreter','latex');
title('Wheels Angular Velocity Error (KF)');
ylim([-0.5 0.5]);
subplot(3,1,3)
plot(t_plot,y(4,:),t_plot,y(5,:),t_plot,y(6,:));
h = legend('$\delta w1$','$\delta w2$','$\delta w3$');
set(h,'Interpreter','latex');
title('Wheels Angular Velocity Error (Encoder)');
ylim([-0.5 0.5]);
