%Simulations for Rolling in a Trajectory
%ME561 -- Final Project 
%Reaction Wheel Ball Bot
%Team:
    %Matthew Porter
    %Isaac Spiegel
    %Yunxiang Xu
    %Ding Zhang

close all
clc

%%
% Non-Linear Simulation
clear xout x_error input Tn  Xn x_ekf t_plot


C = [zeros(6,4) eye(6)]; %output function



ts=0;

xout(1,:)=xsi+[1,0,0,0,2*pi,0,0,-0.2876,0,0];

% initial state for kalman filter
x_pre = xsi';
P_pre = zeros(10);


for i=1:(time/n)
    fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b');
    fprintf('time step %d',i);
    x_ref = [cos(pi*ts),sin(pi*ts),0,0,2*pi,0,0,-0.2876,0,0];
    A = A_dis{i};  %state matrix
    B = disB(ts,n);%input matrix
    
    x_last=xout(length(xout(:,1)),:);
    x_error(i,:)=x_last-x_ref;
    
% output function
    x_state = x_error(i,:)';
    y(:,i) = C*x_state + v(i,:)';
% filtering
    K = P_pre*C'/(C*P_pre*C' + R);
    x_ekf(:,i) = x_pre + K*(y(:,i) - C*x_pre);
    P_ekf = (eye(10) - K*C)*P_pre;
    
    y(:,i) = C*(x_state + x_ref') + v(i,:)';
% LQR
     A_lqr = cell(N,1);
    for j = 1:N
        A_lqr{j} = A_dis{i+j-1};
    end
    input(i,:)=transpose(LQ_reg( A_lqr,ts,x_ekf(:,i)',n,N ));
    uk = input(i,:)';
% prediction
    x_pre = A*x_ekf(:,i)+B*uk;
    P_pre = A*P_ekf*A'+Q;
    
% simulation
    fun=@(t,x)eomfun(t,x,input(i,:));
    [Tn,Xn] = ode45(fun,[ts,ts+n],x_last);
    ts=ts+n;
    x_temp = Xn(length(Xn(:,1)),:)+w(i,:);
    xout=[xout;x_temp];
end
xout = xout(1:total_step,:);
t_plot = 0:n:time-n;
%%
%Plots for Non-Linear Simulation
trajSimPlot;
% figure(4)
% subplot(2,1,1)
% plot(T,xout(:,1:4))
% h=legend('$a$','$b$','$c$','$d$');
% set(h,'Interpreter','latex')
% title('Quaternion Values')
% xlabel('Time (s)')
% ylabel('Values')
% ylim([-1,1])
% 
% subplot(2,1,2)
% plot(T,xout(:,5:7))
% h=legend('$p$','$q$','$r$');
% set(h,'Interpreter','latex')
% title('Body Fixed Angular Velocities')
% xlabel('Time (s)')
% ylabel('(rad/s)')
% hold on
% plot([0,3],[2.2*pi,2.2*pi],'--k',[0,3],[1.8*pi,1.8*pi],'--k')
% 
% %removed
% % subplot(3,1,3)
% % plot(T,xout(:,8:10),'--')
% % h=legend('$w_1$','$w_2$','$w_3$');
% % set(h,'Interpreter','latex')
% % title('Reaction Wheel Angular Velocities')
% % xlabel('Time (s)')
% % ylabel('(rad/s)')
% 
% figure(5)
% subplot(3,1,1)
% plot(0:0.01:2.99,input)
% h=legend('$T_1$','$T_2$','$T_3$');
% set(h,'Interpreter','latex')
% title('Torque Inputs')
% xlabel('Time (s)')
% ylabel('Torque (N/m)')
% 
% subplot(3,1,2)
% plot(0:0.01:2.99,X(:,1:4))
% h=legend('$\delta a$','$\delta b$','$\delta c$','$\delta d$');
% set(h,'Interpreter','latex')
% title('Quaternion Error')
% xlabel('Time (s)')
% ylabel('Values')
% 
% subplot(3,1,3)
% plot(0:0.01:2.99,X(:,5:7))
% h=legend('$\delta p$','$\delta q$','$\delta r$');
% set(h,'Interpreter','latex')
% title('Body Fixed Angular Velocity Error')
% xlabel('Time (s)')
% ylabel('(rad/s)')
% 
% %%
% qa=xout(:,1);
% qb=xout(:,2);
% qc=xout(:,3);
% qd=xout(:,4);
% p=xout(:,5);
% q=xout(:,6);
% r=xout(:,7);
% pos(1,:)=[0,0];
% for i=1:length(qa)-1
%     R=[qa(i)^2+qb(i)^2-qc(i)^2-qd(i)^2  2*qc(i)*qb(i)-2*qa(i)*qd(i)  2*qb(i)*qd(i)+2*qa(i)*qc(i);
%        2*qb(i)*qc(i)+2*qa(i)*qd(i) qa(i)^2-qb(i)^2+qc(i)^2-qd(i)^2 2*qc(i)*qd(i)-2*qa(i)*qb(i);
%        2*qb(i)*qd(i)-2*qa(i)*qc(i)  2*qc(i)*qd(i)+2*qa(i)*qb(i)  qa(i)^2-qb(i)^2-qc(i)^2+qd(i)^2];
%    w=transpose(R)*[p(i);q(i);r(i)];
%    pos(i+1,1)=pos(i,1)+w(2)*0.15*0.01;
%    pos(i+1,2)=pos(i,2)-w(1)*0.15*0.01;
% end
% 
% figure(6)
% s=0:0.01:pi/50;
% plot(-pos(:,2),pos(:,1),[0,2.82],[0,0.246],'--r', 50*sin(s),50*cos(s)-50,'--k',...
%     [0,2.82],[0,-0.246],'--r',50*sin(s),-50*cos(s)+50,'--k')
% axis equal
% xlim([0,2.8])
% ylim([-0.5,0.5])
% xlabel('y-Displacement (m)')
% ylabel('x-Displacement (m)')
% title('Linear Displacement of Ball in World Frame')
% h=legend('$Displacement$','$\pm 5^{\circ} Heading$','$0.02 m^{-1} Curvature$','location','southwest');
% set(h,'Interpreter','latex')
