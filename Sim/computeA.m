
%parameters
n=0.01; %step size of discretized version
N=6; %finite horizon size
time=3; %simulation end time
total_step = time/n;
% state matrix
A_dis = cell(total_step + N,1);
disp('computing A matrix...');
for i = 1: total_step + N
    A_dis{i} = disA((i-1)*n,n);
end