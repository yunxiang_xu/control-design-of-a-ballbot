function A = disA(t,Ts)    
    [~,A1]=ode45(@(t,x)a(t)*x,[t,t+Ts],[1 0 0 0 0 0 0 0 0 0]);
    [~,A2]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 1 0 0 0 0 0 0 0 0]);
    [~,A3]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 1 0 0 0 0 0 0 0]);
    [~,A4]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 1 0 0 0 0 0 0]);
    [~,A5]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 1 0 0 0 0 0]);
    [~,A6]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 0 1 0 0 0 0]);
    [~,A7]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 0 0 1 0 0 0]);
    [~,A8]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 0 0 0 1 0 0]);
    [~,A9]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 0 0 0 0 1 0]);
    [~,A10]=ode45(@(t,x)a(t)*x,[t,t+Ts],[0 0 0 0 0 0 0 0 0 1]);

    A=transpose(...
    [A1(length(A1(:,1)),:); A2(length(A2(:,1)),:);A3(length(A3(:,1)),:);
     A4(length(A4(:,1)),:);A5(length(A5(:,1)),:);A6(length(A6(:,1)),:);
     A7(length(A7(:,1)),:);A8(length(A8(:,1)),:);A9(length(A9(:,1)),:);
     A10(length(A10(:,1)),:)]);
end