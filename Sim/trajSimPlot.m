fprintf('\b\b\b\b\b\b\b\b\b\b\b\b\b\b');
disp('plotting...');

figure(1)
subplot(3,1,1)
plot(0:n:time - n ,xout(1:total_step,1:4))
h=legend('$a$','$b$','$c$','$d$');
set(h,'Interpreter','latex')
title('Quaternion Values')
xlabel('Time (s)')
ylabel('Values')
xlim([0,3])
ylim([-1,1])



subplot(3,1,2)
plot(t_plot,x_error(:,1:4))
h=legend('$\delta a$','$\delta b$','$\delta c$','$\delta d$');
set(h,'Interpreter','latex')
title('Quaternion Error')
xlabel('Time (s)')
ylabel('Values')
xlim([0,time])
ylim([-0.2 0.2])

subplot(3,1,3)
plot(t_plot,x_ekf(1:4,:));
title('Quaternion Error (KF)')
h=legend('$\delta a$','$\delta b$','$\delta c$','$\delta d$');
set(h,'Interpreter','latex')
xlabel('Time (s)')
ylabel('Values')
xlim([0,time]);
ylim([-0.2 0.2]);


figure(2)
subplot(2,2,1)
plot(t_plot,xout(:,5:7))
h=legend('$p$','$q$','$r$');
set(h,'Interpreter','latex')
title('Body Angular Velocities')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,3])
hold on
plot([0,3],[2.2*pi,2.2*pi],'--k',[0,3],[1.8*pi,1.8*pi],'--k')

subplot(2,2,2)
plot(t_plot,y(1:3,:));
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Body Angular Velocity Error (Gyro)')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])


subplot(2,2,3)
plot(t_plot,x_error(:,5:7))
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Body Angular Velocity Error')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])

subplot(2,2,4)
plot(t_plot,x_ekf(5:7,:));
h=legend('$\delta p$','$\delta q$','$\delta r$');
set(h,'Interpreter','latex')
title('Body Angular Velocity Error (KF)')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])

figure(3)

subplot(2,2,1)
plot(t_plot,xout(:,8:10))
legend('w1','w2','w3')
title('Flywheel Angular Velocities')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])
ylim([-0.8 0.2])

subplot(2,2,2)
plot(t_plot,y(4:6,:))
legend('w1','w2','w3')
title('Flywheel Angular Velocities (Encoder)')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])
ylim([-0.8 0.2])

subplot(2,2,3)
plot(t_plot,x_error(:,8:10))
h = legend('$\delta w1$','$\delta w2$','$\delta w3$');
set(h,'Interpreter','latex');
title('Flywheel Angular Velocity Error')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])
ylim([-0.5 0.3]);

subplot(2,2,4)
plot(t_plot,x_ekf(8:10,:))
legend('w1','w2','w3')
title('Flywheel Angular Velocity Error (KF)')
xlabel('Time (s)')
ylabel('(rad/s)')
xlim([0,time])
ylim([-0.5 0.3]);


figure(4)
plot(t_plot,input)
h=legend('$T_1$','$T_2$','$T_3$');
set(h,'Interpreter','latex')
title('Torque Inputs')
xlabel('Time (s)')
ylabel('Torque (N/m)')
xlim([0,3])



qa=xout(:,1);
qb=xout(:,2);
qc=xout(:,3);
qd=xout(:,4);
p=xout(:,5);
q=xout(:,6);
r=xout(:,7);
pos(1,:)=[0,0];
for i=1:length(qa)-1
    R=[qa(i)^2+qb(i)^2-qc(i)^2-qd(i)^2  2*qb(i)*qc(i)-2*qa(i)*qd(i)  2*qb(i)*qd(i)+2*qa(i)*qc(i);
       2*qb(i)*qc(i)+2*qa(i)*qd(i) qa(i)^2-qb(i)^2+qc(i)^2-qd(i)^2 2*qc(i)*qd(i)-2*qa(i)*qb(i);
       2*qb(i)*qd(i)-2*qa(i)*qc(i)  2*qc(i)*qd(i)+2*qa(i)*qb(i)  qa(i)^2-qb(i)^2-qc(i)^2+qd(i)^2];
   w=transpose(R)*[p(i);q(i);r(i)];
   pos(i+1,1)=pos(i,1)+w(2)*0.15*0.01;
   pos(i+1,2)=pos(i,2)-w(1)*0.15*0.01;
end

figure(5)
s=0:0.01:pi/50;
plot(-pos(:,2),pos(:,1),[0,2.82],[0,0.246],'--r', 50*sin(s),50*cos(s)-50,'--k',...
    [0,2.82],[0,-0.246],'--r',50*sin(s),-50*cos(s)+50,'--k')
axis equal
xlim([0,2.8])
ylim([-0.5,0.5])
xlabel('y-Displacement (m)')
ylabel('x-Displacement (m)')
title('Linear Displacement of Ball in World Frame')
h=legend('$Displacement$','$\pm 5^{\circ} Heading$','$0.02 m^{-1} Curvature$','location','southwest');
set(h,'Interpreter','latex')